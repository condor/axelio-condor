<?php

  // Are we in Localization class? If not break!
  if(!isset($this) || !($this instanceof Localization)) {
    throw new InvalidInstanceError('$this', $this, 'Localization', "File '" . __FILE__ . "' puede ser incluida sólo desde la clase Localization");
  } // if

?>