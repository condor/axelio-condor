<?php 
	require_javascript("og/modules/addUserForm.js");
	require_javascript("og/Permissions.js");
	$genid = gen_id();
	$object = $user;
set_page_title('Registro de Participantes') ?>
<form class="internalForm" action="<?php echo get_url('access', 'registro') ?>" method="post">
<?php tpl_display(get_template_path('form_errors')) ?>

  <p><?php echo "Por favor, ingresa tus datos para registrarte correctamente en el Sistema de Gestión Nacional Cóndor de Canaima Universitario" ?></p>

  <h2><?php echo lang('newbie') ?></h2>

<!-- Nombre de usuario por defecto 
  <div>
    <?php echo label_tag(lang('username'), 'Username', true) ?>
    <?php echo text_field('form[username]', array_var($form_data, 'username'), array('id' => 'username', 'class' => 'medium')) ?>
  </div>
-->
    	<!-- username -->
    <div>
    <?php echo label_tag(lang('username'), $genid.'userFormName', true) ?>
      <?php echo text_field('user[username]', array_var($user_data, 'username'), 
    	array('class' => 'medium', 'id' => $genid.'userFormName', 'tabindex' => '100','onchange'=>'og.determinePersonalwsName(this, \'' . escape_single_quotes(new_personal_project_name()) .'\')')) ?>
    </div>

  <!-- Correo por defecto
  <div>
    <?php echo label_tag(lang('email address'), 'Email', true) ?>
    <?php echo text_field('form[email]', array_var($form_data, 'email'), array('id' => 'email', 'class' => 'long')) ?>
  </div>
  -->
    	<!-- email -->
    <div>
      <?php echo label_tag(lang('email address'), 'userFormEmail', true) ?>
      <?php echo text_field('user[email]', array_var($user_data, 'email'), 
    	array('class' => 'email', 'id' => 'long', 'tabindex' => '200')) ?>
    </div>

<!-- Contraseña -->
    <div id="userFormPasswordInputs">
      <div>
        <?php echo label_tag(lang('password'), 'userFormPassword', true) ?>
        <?php echo password_field('user[password]', null, array('id' => 'userFormPassword', 'tabindex' => '900')) ?>
      </div>
      
      <div>
        <?php echo label_tag(lang('password again'), 'userFormPasswordA', true) ?>
        <?php echo password_field('user[password_a]', null, array('id' => 'userFormPasswordA', 'tabindex' => '1000')) ?>
      </div>
    </div>

<!-- Compañía -->
    
  <div>
    <?php echo label_tag(lang('region'), 'region', true) ?>
    <?php echo select_company('form[company_name]', array_var($form_data, 'company_name'), array('id' => 'companyName', 'class' => 'long')) ?>
  </div>

<!-- Envío de notificacón al correo -->
    <div style="margin-top:10px">
    <label class="checkbox">
    <?php echo checkbox_field('user[send_email_notification]', array_var($user, 'send_email_notification', 1), array('id' => $genid . 'notif', 'tabindex' => '1050')) ?>
    <?php echo lang('send new account notification')?>
    </label>
    </div>

<!-- contraseña por defecto
  <div>
    <?php echo label_tag(lang('password'), 'Password', true) ?>
    <?php echo password_field('form[password]', null, array('id' => 'password', 'class' => 'medium')) ?>
  </div>

  <div>
    <?php echo label_tag(lang('password again'), 'PasswordA', true) ?>
    <?php echo password_field('form[password_a]', null, array('id' => 'password_a', 'class' => 'medium')) ?>
  </div>
-->

  <input type="hidden" name="form[submited]" value="submited" />
  
  <?php echo submit_button('submit') ?>
  
</form>
