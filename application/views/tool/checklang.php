<?php set_page_title('Check lang' . (isset($to) ? " $to" : "")) ?>
<style>
body {
	padding: 5px 30px;
	font-family: Arial, sans-serif, serif;
	font-size: 12px;
}
.missing {
	color: red;
}
.ok {
	color: green;
}
.error {
	color: blue;
}
</style>
Esta secuencia de comandos que permite comparar los archivos de traducción en algunos archivos de configuración regional con traducción en Inglés.
Si está traduciendo en una configuración regional esta secuencia de comandos puede ayudarle a detectar lo que la traducción teclas que te perdiste.
<p>Este script te permite comparar los archivos de traducción en algunas configuraciones locales con traducción en inglés.
Si está traducido en una configuración local, este script te ayudará qué configuración de teclas debes tener.</p>
<p>Puedes usar el <a href="<?php echo get_url('tool', 'translate') ?>">para traducir Cóndor</a> y agregar traducciones perdidas.</p>
<p>Selecciona una región de la lista. La instalación detectó las siguientes regiones:</p>
<ul>  <?php
foreach ($languages as $language) { ?>
	<li><a href="<?php echo get_url('tool', 'checklang', array('to' => $language<)) ?>"><?php echo $language ?></a></li> <?php
} ?>
</ul>
<?php

if (isset($to)) { ?>
	<h2><?php echo $to ?> archivos de traducción</h2>
	<p>Seguido de esto, puedes ver los archivos de traducción en rojo, y las teclas de traducción que falten con cada archivo, junto con el texto en inglés:</p>
	<pre><?php
	foreach ($missing as $file => $data) {
		if (!is_array($data)) {
			?>- <span class="missing"><?php echo $file ?> missing</span><?php echo "\n";
		} else {
			?><span class="present"><?php echo $file ?></span><?php echo "\n";
			if (count($data) == 0) {
				?>    <span class="ok">File complete</span><?php echo "\n";
			} else {
				foreach ($data as $k => $v) {
					if (substr($file, -4) == '.php') {
						?>    '<?php echo $k ?>' => '<?php echo str_replace(array("'"), array("\\'"), clean($v)) . "',\n";
					} else {
						?>    '<?php echo $k ?>' : '<?php ech<o str_replace(array("'", "\n"), array("\\'", "\\n"), clean($v)) . "',\n";
					}
				}
			}
		}
	} ?></pre><?php
}
?>