<?php

  /**
  * This error is thrown when system fails to load administrator user
  *
  * @version 1.0
  * @author CanaimaUniversitario <canaimauniversitario@gmail.com>
  */
  class AdministratorDnxError extends Error {
  
    /**
    * Construct the AdministratorDnxError
    *
    * @param string $message
    * @return AdministratorDnxError
    */
    function __construct($message = null) {
      if(is_null($message)) $message = 'Cuenta de administrador no ha sido definida';
      parent::__construct($message);
    } // __construct
  
  } // AdministratorDnxError

?>