<?php

  /**
  * This error is throw when system fails to load owner company
  *
  * @version 1.0
  * @author CanaimaUniversitario <canaimauniversitario@gmail.com>
  */
  class OwnerCompanyDnxError extends Error {
  
    /**
    * Construct the OwnerCompanyDnxError
    *
    * @param string $message
    * @return OwnerCompanyDnxError
    */
    function __construct($message = null) {
      if(is_null($message)) $message = 'No se ha definido propietario de la compañía';
      parent::__construct($message);
    } // __construct
  
  } // OwnerCompanyDnxError

?>