<?php

  if(!version_compare(phpversion(), '5.0', '>=')) {
    die('<strong>Error de instalación:</strong> Cóndor necesita PHP5 para que todas sus funciones estén activas. Tu versión de PHP es: ' . phpversion());
  } // if
  if(!defined('PUBLIC_FOLDER')) {
    define('PUBLIC_FOLDER', 'public'); // this file can be included through public/index.php
  } // if
  require 'init.php';
  
?>