<?php

  /**
  * Abstract output
  *
  * @author CanaimaUniversitario <canaimauniversitario@gmail.com>
  */
  abstract class Output {
    
    /**
    * Print a specific message to a specific output
    *
    * @param string $message
    * @param boolean $is_error
    * @return void
    */
    abstract function printMessage($message, $is_error = false);
    
  } // Output

?>