<h1 class="pageTitle"><span>Paso <?php echo $current_step->getStepNumber() ?>:</span> Bienvenido</h1>
<p>Cóndor es una web de trabajo desarrollado bajo código abierto. Esto incluye documentos, tareas y presentaciones en editores web para el buen manejo de proyectos. Cóndor es:</p>
<ul>
  <li><strong>Fácil de usar</strong> - posee un set de herramientas básicas para el trabajo</li>
  <li><strong>Fácil de instalar</strong> - estás aquí, sólo sigue las instrucciones</li>
  <li><strong>100% libre</strong> - libre para todo, salvo para usos comerciales</li>
  <li><strong>Basado en páginas web</strong> - después de la instalación, sólo necesitarás un navegador web</li>
</ul>

<h2>Pasos de instalación:</h2>
<ol>
<?php foreach($installer->getSteps() as $this_step) { ?>
  <li><?php echo clean($this_step->getName()) ?></li>
<?php } // foreach ?>
</ol>
<p>Pronto habrás terminado.</p>