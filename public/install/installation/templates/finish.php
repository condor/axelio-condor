<h1 class="pageTitle"><span>Paso <?php echo $current_step->getStepNumber() ?>:</span> Finalización</h1>
<p>Proceso de instalación:</p>
<?php if(isset($status_messages)) { ?>
<ul>
<?php foreach($status_messages as $status_message) { ?>
  <li><?php echo $status_message ?></li>
<?php } // foreach ?>
</ul>
<?php } // if ?>

<?php if(isset($all_ok) && $all_ok) { ?>
<h1>¡Lo has logrado!</h1>
<p>Has instalado Cóndor <strong>exitosamente</strong>. Ve a<a href="<?php echo $absolute_url ?>" onclick="window.open('<?php echo $absolute_url ?>'); return false;"><?php echo clean($absolute_url) ?></a> y comienza a manejar tus proyectos (Cóndor le preguntará si quiere crear un usuario administrador y deberá proveer algunos detalles de su primera empersa).</p>
<p><strong>Visita <a href="http://universitario.canaima.net.ve/wiki/doku.php?id=3_propuestas_y_proyectos:11_sistema_de_gestion_general">Cóndor</a> para tener información, noticias, actualizaciones y soporte</strong>.
Visita nuestro <a href="http://universitario.canaima.net.ve/wiki/doku.php?id=6%20contacto%20cu:irc_chat">Canal IRC</a> para que puedas conversar con usuarios de Canaima Universitario. ¡Gracias!</p>
<?php } // if ?>