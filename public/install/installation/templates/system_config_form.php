<h1 class="pageTitle"><span>Paso <?php echo $current_step->getStepNumber() ?>:</span> Configuración del Sistema</h1>
<table class="formBlock">
  <tr>
    <th colspan="2">Conexión de base de datos</th>
  </tr>
  
  <tr>
    <td class="optionLabel"><label for="configFormDatabaseType">Base de datos de tipo:</label></td>
    <td>
      <select name="config_form[database_type]" id="configFormDatabaseType">
        <option value="mysql">MySQL</option>
        <option value="pdo_mysql">PDO MySQL</option>
      </select>
    </td>
  </tr>
  
  <tr>
    <td class="optionLabel"><label for="configFormDatabaseHost">Nombre del host:</label></td>
    <td><input type="text" name="config_form[database_host]" id="configFormDatabaseHost" value="<?php echo array_var($config_form_data, 'database_host', 'localhost') ?>" /></td>
  </tr>
  
  <tr>
    <td class="optionLabel"><label for="configFormDatabaseUser">Nombre de usuario:</label></td>
    <td><input type="text" name="config_form[database_user]" id="configFormDatabaseUser" value="<?php echo array_var($config_form_data, 'database_user') ?>" /></td>
  </tr>
  
  <tr>
    <td class="optionLabel"><label for="configFormDatabasePass">Contraseña:</label></td>
    <td><input type="password" name="config_form[database_pass]" id="configFormDatabasePass" value="<?php echo array_var($config_form_data, 'database_pass') ?>" /></td>
  </tr>
  
  <tr>
    <td class="optionLabel"><label for="configFormDatabaseName">Nombre de base de datos:</label></td>
    <td><input type="text" name="config_form[database_name]" id="configFormDatabaseName" value="<?php echo array_var($config_form_data, 'database_name') ?>" /></td>
  </tr>
  
  <tr>
    <td class="optionLabel"><label for="configFormDatabasePrefix">Prefijo de las tablas:</label></td>
    <td><input type="text" name="config_form[database_prefix]" id="configFormDatabasePrefix" value="<?php echo array_var($config_form_data, 'database_prefix', 'og_') ?>" /></td>
  </tr>
  
  <tr>
    <td class="optionLabel"><label for="configFormDatabaseEngine">Motor de base de datos:</label></td>
    <td>
    	<select name="config_form[database_engine]" id="configFormDatabaseEngine">
	        <option value="InnoDB">InnoDB</option>
	        <option value="MyISAM">MyISAM</option>
	    </select>
    </td>
  </tr>
  
  <tr>
    <td colspan="2">
    <b>Nota:</b> InnoDB es un motor altamente recomendable. Utilice MyISAM sólo como último recurso, y bajo su propio riesgo, si su base de datos no es compatible con InnoD.<br /><br />
    <b>Note:</b> Si la base de datos no existe, entonces Cóndor intentará crearla. Por otra parte, si no tiene permisos para crear datos nuevos en la base de datos, entonces usted deberá crearla manualmente para poder continuar.
    </td>
  </tr>
</table>

<table class="formBlock">
  <tr>
    <th colspan="2">Otras configuraciones</th>
  </tr>

  <tr>
    <td class="optionLabel"><label for="configFormAbsoluteUrl">URL del script:</label></td>
    <td><input type="text" name="config_form[absolute_url]" id="configFormAbsoluteUrl" value="<?php echo array_var($config_form_data, 'absolute_url', $installation_url) ?>" /></td>
  </tr>
</table>